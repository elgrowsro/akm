<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'name'=>'АМК-Екатеринбург',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'gii' => [
            'class' => 'yii\gii\Module',
            'allowedIPs' => [
                '192.168.153.*',
            ],
        ],
    ],
    // 'as access' => [
    //     'class' => 'mdm\admin\components\AccessControl',
    //     'allowActions' => [
    //         'user/*',
    //         'aktion/*',
    //         'category/*',
    //         'carcass/*',
    //         'page/*',
    //         'car/*',
    //         'site/*',
    //         'gii/*', 
    //         // The actions listed here will be allowed to everyone including guests.
    //         // So, 'admin/*' should not appear here in the production, of course.
    //         // But in the earlier stages of your development, you may probably want to
    //         // add a lot of actions here until you finally completed setting up rbac,
    //         // otherwise you may not even take a first step.
    //     ]
    // ],
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'transport' => [
            'class' => 'Swift_SmtpTransport',
            'host' => 'mail.navbase.ru', //вставляем имя или адрес почтового сервера
            'username' => 'admin@navbase.ru', 
            'password' => 'S0DhPScZII2VhbWNONdH',
            'port' => '587',
            'encryption' => '',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'cart' => [
            'class' => 'yz\shoppingcart\ShoppingCart',
        ],
        'defaultRoute' => 'site/index',
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            // 'enableStrictParsing'=>true,
            // 'rules' => [
            //     [
            //         'pattern' => 'aktion/index',
            //         'route' => 'aktion/index',
            //         'suffix'=>'.html'
            //     ],
            //     [
            //         'pattern'=>'<controller>/<action>',
            //         'route'=>'<controller>/<action>',
            //         'suffix'=>'.html'
            //     ],
            //     [
            //         'pattern'=>'<module>/<controller>/<action>',
            //         'route'=>'<controller>/<action>',
            //         'suffix'=>'.html'
            //     ],
            //     [
            //         'pattern'=>'<module>/<controller>/<action>/<id:\id+>',
            //         'route'=>'<controller>/<action>',
            //         'suffix'=>'.html'
            //     ],
            //     [
            //         'pattern'=>'',
            //         'route'=>'site/index',
            //         'suffix'=>''
            //     ],
            // ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}