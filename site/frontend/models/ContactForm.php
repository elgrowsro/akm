<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $car;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            // [['name', 'email', 'subject', 'body'], 'required'],
            [['name', 'phone'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            [['car'], 'string', 'max' => 255],
            // verifyCode needs to be entered correctly
            // ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Ваше имя',
            'subject' => 'Тема сообщения',
            'phone' => 'Телефон',
            'body' => 'Сообщение',
            'car' => 'Автомобиль',
            'verifyCode' => 'Код подтверждения',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param  string  $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail($email)
    {
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([$email => $this->name])
            ->setSubject('Заказ звонка с сайта АМК Екатеринбург')
            ->setTextBody('Пользователь '.$this->name.' желает,чтоб ему перезвонили по номеру: '.$this->phone) 
            ->send();
    }

    public function sendTestdrive($email)
    {
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([$email => $this->name])
            ->setSubject('Заказ тестдрайва с сайта АМК Екатеринбург')
            ->setTextBody('Пользователь '.$this->name.', тел: '.$this->phone.' - заказал услугу Тестдрайв на автомобиль: '.$this->car) 
            ->send();
    }
}
