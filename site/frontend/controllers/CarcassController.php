<?php

namespace frontend\controllers;

use Yii;
use common\models\Carcass;
use common\models\Aktion;
use common\models\Imgcarcass;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\ContactForm;

/**
 * CarcassController implements the CRUD actions for Carcass model.
 */
class CarcassController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Carcass models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Carcass::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Carcass model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $call = new ContactForm();
        $testdrive = new ContactForm();

        $exterierProvider = new ActiveDataProvider([
            'query' => Imgcarcass::find()->where(['type'=>0,'carcassId'=>$model->id]),
            'pagination' => [
                'pageSize' => 60,
            ],
        ]);

        $model = $this->findModel($id);
        $interierProvider = new ActiveDataProvider([
            'query' => Imgcarcass::find()->where(['type'=>1,'carcassId'=>$model->id]),
            'pagination' => [
                'pageSize' => 60,
            ],
        ]);

        $aktions = Aktion::find()->where(['not',['imginfobox'=> '']])->orderBy('enddate ASC')->all();

        if ($call->load(Yii::$app->request->post()) && $call->validate()) {
            if ($call->sendEmail(Yii::$app->params['adminEmail'])) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        if ($testdrive->load(Yii::$app->request->post()) && $testdrive->validate()) {
            if ($testdrive->sendTestdrive(Yii::$app->params['adminEmail'])) {
                return $this->redirect(['view', 'id' => $model->id]);
            } 
        }

        return $this->render('view', [
            'model' => $model,
            'call' => $call,
            'testdrive' => $testdrive,
            'aktions' => $aktions,
            'exterierProvider' => $exterierProvider,
            'interierProvider' => $interierProvider,
        ]);
    }


    /**
     * Finds the Carcass model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Carcass the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Carcass::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
