<?php

namespace frontend\controllers;

use Yii;
use common\models\Car;
use common\models\Slide;
use common\models\Aktion;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\ContactForm;


/**
 * CarController implements the CRUD actions for Car model.
 */
class CarController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Car models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Car::find(),
        ]);

        $aktion = Aktion::find()->where(['not',['imginfobox'=> null]])->orderBy('enddate ASC')->one();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'aktion' => $aktion,
        ]);
    }

    /**
     * Displays a single Car model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $images = Slide::findAll(['carId' => $model->id]);
        $call = new ContactForm();

 /*        if ($call->load(Yii::$app->request->post()) && $call->validate()) {
            $call = new ContactForm();
            if ($call->sendEmail(Yii::$app->params['adminEmail'])) {
                return $this->redirect(['index']);
            }
        }*/

        return $this->renderAjax('view', [
            'model' => $model,
            'images' => $images,
            // 'call' => $call,
        ]);
    }

    /**
     * Finds the Car model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Car the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Car::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
