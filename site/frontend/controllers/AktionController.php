<?php

namespace frontend\controllers;

use Yii;
use common\models\Aktion;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use sanex\simplefilter\SimpleFilter;
use frontend\models\ContactForm;

/**
 * AktionController implements the CRUD actions for Aktion model.
 */
class AktionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Aktion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Aktion::find(),
        ]);

        $aktion = Aktion::find()->where(['not',['imginfobox'=> null]])->orderBy('enddate ASC')->one();

        $model = new Aktion;

        $query = new \yii\db\ActiveQuery($model);
        $query->orderBy('id'); 

        $ajaxViewFile = '@app/views/aktion/index-ajax';

        $filter = SimpleFilter::getInstance();
        $filter->setParams([
            'model' => $model,
            'query' => $query,
            'useAjax' => true,
            // 'useCache' => true,
            'useDataProvider' => true,
        ]);

        return $this->render('index', [
            'filter' => $filter,
            'ajaxViewFile' => $ajaxViewFile,
            'aktion' => $aktion,
            // 'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Aktion model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $call = new ContactForm();
        $testdrive = new ContactForm();

        if ($call->load(Yii::$app->request->post()) && $call->validate()) {
            if ($call->sendEmail(Yii::$app->params['adminEmail'])) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        if ($testdrive->load(Yii::$app->request->post()) && $testdrive->validate()) {
            if ($testdrive->sendTestdrive(Yii::$app->params['adminEmail'])) {
                return $this->redirect(['view', 'id' => $model->id]);
            } 
        }

        return $this->render('view', [
            'model' => $model,
            'testdrive' => $testdrive,
            'call' => $call,
        ]);
    }

    /**
     * Finds the Aktion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Aktion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Aktion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
