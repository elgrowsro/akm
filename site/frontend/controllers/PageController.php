<?php

namespace frontend\controllers;

use Yii;
use common\models\Page;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\ContactForm;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Creates a new Page model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCredit()
    {
        $model = $this->findModel(1);
        $call = new ContactForm();

        if ($call->load(Yii::$app->request->post()) && $call->validate()) {
            if ($call->sendEmail(Yii::$app->params['adminEmail'])) {
                return $this->redirect(['credit']);
            }
        }

        return $this->render('credit', [
                'model' => $model,
                'call' => $call,
            ]);
    }

    public function actionService()
    {
        $model = $this->findModel(2);
        $testdrive = new ContactForm();

        if ($testdrive->load(Yii::$app->request->post()) && $testdrive->validate()) {
            if ($testdrive->sendTestdrive(Yii::$app->params['adminEmail'])) {
                return $this->redirect(['service']);
            } 
        }

        return $this->render('service', [
            'model' => $model,
            'testdrive' => $testdrive,
        ]);
    }
   
    public function actionContacts()
    {
        $model = $this->findModel(3);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect('index');
        } else {
            return $this->render('contacts', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
