<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Модельный ряд';
?>
<div class="mainWrapper mainWrapper_bg mainWrapper_height-auto" style="background-image: url(&quot;/images/slide.jpg&quot;)">
    <?= $this->render('//layouts/header_nav') ?>
    <div class="page-title">
        <div class="container-content">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item"><a href="/" target="">Главная</a></li> -
                <li class="breadcrumbs__item"><span>Модельный ряд</span></li>
            </ul>
            <h1 class="page-title__h1">Модельный ряд</h1>
        </div>
    </div>
    <div class="container-content">
        <div class="actions-infobox">
            <div class="actions-infobox-slider">
                <div id="actions-infobox-slider">
                    <?php if ($aktions != NULL): ?>
                        <?php foreach ($aktions as $key => $aktion): ?>
                            <div class="actions-infobox__cont">
                                <div class="uk-grid uk-grid-medium">
                                    <div class="uk-width-large-4-10 uk-text-center">
                                        <div class="countdown" data-enddate="<?= date('Y, m, d',$aktion->enddate) ?>"></div>
                                        <h2 class="actions-infobox__title"><?= $aktion->title ?></h2>
                                        <p class="actions-infobox__anons"><?php $text = substr($aktion->description,0,200); echo $text."..."; ?></p>
                                        <?= Html::a('Подробнее', ['aktion/view','id'=>$aktion->id], ['class' => 'btn btn_ghost uk-button','target'=>'_blank']) ?>
                                    </div>
                                    <div class="uk-width-large-6-10">
                                        <?= Html::img('@web/images/'.$aktion->imginfobox,['alt'=>$aktion->title]) ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach ?>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?= ListView::widget([
  'dataProvider' => $dataProvider,
  'itemView' => '_items',
  'options'=>['class' => 'models'],
  'layout' => "{items}",
]) ?>