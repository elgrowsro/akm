<?php 
use common\models\Carcass;
use yii\helpers\Html;
?>
<div class="models__item">
    <div class="container-content container-content_padding-all">
        <div class="title" id="anchor-<?= $model->id ?>"><?= $model->title ?></div>
        <div class="models-list">
            <?php foreach (Carcass::find()->where(['categoryId'=>$model->id])->all() as $key => $carcass): ?>
                <div class="models-list__item figure uk-overlay uk-overlay-hover">
                    <?= Html::a(Html::img('@web/images/img-model.jpg',['alt'=>'','width'=>'233px','height'=>'321px']).
                        '<figcaption class="uk-overlay-panel uk-overlay-background uk-overlay-ignore uk-flex uk-flex-bottom uk-flex-middle uk-text-center">
                            <div>
                                <h3 class="models-list__title">'.$carcass->title.'</h3>
                                <div class="models-list__cont">'.$carcass->description.'</div>
                            </div>
                        </figcaption>', 
                        ['carcass/view','id'=>$carcass->id], ['target' => '_blank']) ?>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>