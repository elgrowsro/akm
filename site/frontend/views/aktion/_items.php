<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use common\models\Aktion;

?>
<div>
    <figure class="actions-list__item uk-overlay uk-overlay-hover">
    	<?= Html::a(
	    	'<div class="countdown" data-enddate="'.date('Y, m, d',$model->enddate).'"></div>'
	    	.Aktion::getImage($model->id).
	        '<figcaption class="uk-overlay-panel uk-overlay-bottom uk-ignore">'.$model->title.'</figcaption>', 
	    	['view','id'=>$model->id], ['class' => 'actions-list__link']) ?>
    </figure>
</div>