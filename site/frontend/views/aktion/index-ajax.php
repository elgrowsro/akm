<?php 

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\web\View;
?>

<?php yii\widgets\Pjax::begin(); ?>
    <?= ListView::widget([
      'dataProvider' => $simpleFilterData,
      'itemView' => '_items',  
      'options'=>['class' => 'actions-list__ul uk-grid uk-grid-small uk-grid-width-large-1-4 uk-grid-width-medium-1-2 uk-grid-width-small-1-1'],
      'layout' => "{items}",
    ]) ?>
<?php yii\widgets\Pjax::end(); ?>