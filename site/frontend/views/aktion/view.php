<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
/* @var $model common\models\Aktion */

$this->title = $model->title;
?>

<div class="mainWrapper mainWrapper_bg" style="background-image: url(/images/bg-action.jpg)">
<?= $this->render('//layouts/header_nav') ?>
    <div class="page-title">
        <div class="container-content">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item"><a href="/" target="">Главная</a></li> -
                <li class="breadcrumbs__item"><a href="" target="">Акции</a></li> -
                <li class="breadcrumbs__item"><span><?= $model->title ?></li>
            </ul>
            <h1 class="page-title__h1"><?= $model->title ?></h1></div>
    </div>
    <div class="container-content container-content_padding-all">
        <div class="action-body">
            <div class="bg-box uk-margin">
                <div class="action-content">
                    <p><?= $model->description ?></p>
                </div>
            </div>
            <div class="action-timer uk-text-center uk-margin">
                <div class="action-timer__title">До конца акции осталось</div>
                <div class="countdown" data-enddate="2016, 11, 31"></div>
            </div>
            <div class="bg-box uk-margin">
                <div class="uk-form uk-form-stacked">
                    <?php $form = ActiveForm::begin(['id'=>'call-form']); ?>
                        <div class="uk-form-row">
                            <div class="uk-grid uk-grid-width-medium-1-2">
                                <div class="uk-form-controls">
                                    <?= $form->field($call, 'name')->textInput(['class' => 'uk-width-1-1','placeholder'=>'Введите ваше имя...'])->label('Ваше имя',['class'=>'uk-form-label']) ?>
                                </div>
                                <div class="uk-form-controls">
                                    <?= $form->field($call, 'phone')->textInput(['class' => 'uk-width-1-1 js-mask-phone','placeholder'=>'+7 (900) 000-00-00'])->label('Контактный телефон',['class'=>'uk-form-label']) ?>
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <?= Html::submitButton('Записаться на тест-драйв', ['class'=>'uk-button uk-width-1-1']) ?>
                            </div>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="uk-modal modal" id="modal-spinner">
    <div class="uk-modal-dialog uk-modal-dialog-lightbox modal-in">
        <a class="uk-modal-close uk-close uk-close-alt" href=""></a>
        <div class="uk-hidden" id="modal-dialog-content"></div>
        <div class="uk-modal-caption"></div>
        <div class="uk-modal-spinner"></div>
    </div>
</div>
<div class="sidebar uk-offcanvas" id="sidebar">
    <div class="uk-offcanvas-bar">
        <div class="sidebar__wrapp uk-flex">
            <div class="sidebar__bar uk-flex uk-flex-column uk-flex-space-between">
                <div></div>
                <div class="sidebar__logo">
                    <a href="/"><img src="/images/logo.png" alt=""></a>
                </div>
                <nav class="sidebar__nav">
                    <ul class="menu-sidebar">
                        <li class="menu-sidebar__item"><a class="menu-sidebar__link" href="#anchor-0">Акции</a></li>
                        <li class="menu-sidebar__item"><a class="menu-sidebar__link" href="#anchor-1">Модельный ряд</a></li>
                        <li class="menu-sidebar__item active"><a class="menu-sidebar__link" href="#anchor-2">Кредит</a></li>
                        <li class="menu-sidebar__item"><a class="menu-sidebar__link" href="#anchor-3">Сервис</a></li>
                        <li class="menu-sidebar__item"><a class="menu-sidebar__link" href="#anchor-4">Трейд-ин</a></li>
                        <li class="menu-sidebar__item"><a class="menu-sidebar__link" href="#anchor-5">Контакты</a></li>
                    </ul>
                </nav>
                <div></div>
                <div class="sidebar__bottom">
                    <div class="uk-margin"><a href="tel:+7 (343) 211 10 00 "><i class="icon sprite-i-phone-vol"></i> +7 (343) 211 10 00</a></div>
                    <div class="uk-margin">
                        <button class="uk-button" type="button" data-uk-modal="{target:'#modal-call'}">Бесплатный звонок</button>
                    </div>
                    <div class="uk-margin"><a href="#modal-map" data-uk-modal title="title"><i class="uk-icon-map-marker"></i> Как проехать?</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
