<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\widgets\ListView;

?>
<div class="mainWrapper mainWrapper_bg mainWrapper_height-auto" style="background-image: url(&quot;/images/slide.jpg&quot;)">
    <?= $this->render('//layouts/header_nav') ?>
    <div class="page-title">
        <div class="container-content">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item"><a href="/" target="">Главная</a></li> -
                <li class="breadcrumbs__item"><span>Акции</span></li>
            </ul>
            <h1 class="page-title__h1">Акции</h1>
        </div>
    </div>
    <div class="container-content">
        <div class="actions-infobox">
            <div class="actions-infobox__filter">
                <div class="uk-form">
                    <?= $filter->setFilter([
                        [
                            'property' => 'type',
                            'caption' => 'Type',
                            'values' => [
                                '0',
                                '1',
                                '2',
                            ],
                            'class' => 'horizontal'
                        ],
                    ]); ?>
                    <form id="" method="" action="">
                        <div class="uk-form-row">
                            <div class="actions-infobox__filter-item">
                                <?= Html::a('Все акции', ['index'], ['class' => 'btn btn_ghost uk-button'/*,'target'=>'_blank'*/]) ?>
                            </div>
                            <div class="actions-infobox__filter-item">
                                <label class="label-check uk-form-label">
                                    <input type="checkbox"><span>Автомобили</span></label>
                            </div>
                            <div class="actions-infobox__filter-item">
                                <label class="label-check uk-form-label">
                                    <input type="checkbox"><span>Кредит</span></label>
                            </div>
                            <div class="actions-infobox__filter-item">
                                <label class="label-check uk-form-label">
                                    <input type="checkbox"><span>Сервис</span></label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="actions-infobox__cont">
                <div class="uk-grid uk-grid-medium">
                    <?php if ($aktion != NULL): ?>
                        <div class="uk-width-large-4-10 uk-text-center">
                            <div class="countdown" data-enddate="<?= date('Y, m, d',$aktion->enddate) ?>"></div>
                            <h2 class="actions-infobox__title"><?= $aktion->title ?></h2>
                            <p class="actions-infobox__anons"><?php $text = substr($aktion->description,0,200); echo $text."..."; ?></p>
                            <?= Html::a('Подробнее', ['view','id'=>$aktion->id], ['class' => 'btn btn_ghost uk-button','target'=>'_blank']) ?>
                        </div>
                        <div class="uk-width-large-6-10">
                            <?= Html::img('@web/images/'.$aktion->imginfobox,['alt'=>$aktion->title]) ?>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-content container-content_padding-all">
    <div class="actions-list">
        <div class="actions-list__title title">Другие акции</div>
        <?= $filter->renderAjaxView($ajaxViewFile); ?>
    </div>
</div>