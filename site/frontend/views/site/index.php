<?php
use backend\assets\AppAsset;
use common\models\Carcass;
use yii\helpers\Html;
use yii\widgets\Menu;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\widgets\ListView;
use yii\bootstrap\Button;
use common\models\Page;

$data = Page::findOne(4);
/* @var $this yii\web\View */

$this->title = 'Акции';
?>
<div class="hm-first-screen mainWrapper uk-flex">
    <div class="hm-first-screen__cont uk-flex uk-flex-column uk-flex-space-between">
        <header class="header header_hm">
            <div class="container-content">
                <div class="uk-flex uk-flex-space-between uk-flex-wrap uk-flex-middle">
                    <div class="header__left">
                        <div class="header__logo">
                            <?= Html::a(Html::img('@web/images/logo.png',['width'=>'40px']),['/']) ?>
                        </div>
                        <?= Html::tag('button', 'Бесплатный звонок', ['class' => 'uk-button','data-uk-modal' => '{target:\'#modal-call\'}']) ?>
                        <?= Html::a('<i class="icon sprite-i-phone-vol"></i> '.$data->c13,'tel:'.$data->c13) ?>
                        <?= Html::a('<i class="icon sprite-i-phone-mobile"></i> '.$data->c21,'tel:'.$data->c21) ?>
                    </div>
                    <div class="header__center">
                        <div class="header__logo">
                            <a href="/"><img src="/images/logo.png"></a>
                        </div>
                    </div>
                    <div class="header__right">
                        <span class="header__adres"><?= $data->c22 ?></span>
                        <a href="#modal-map" data-uk-modal title="title"><i class="uk-icon-map-marker"></i> Как проехать?</a>
                        <?= Html::tag('button', 'Запись на тест-драйв', ['class' => 'uk-button','data-uk-modal' => '{target:\'#modal-testdrive\'}']) ?>
                        <a class="sidebar-tgl" href="#sidebar" data-uk-offcanvas>Меню <i class="uk-icon-bars"></i></a></div>
                </div>
            </div>
        </header>
        <div class="hm-slider-wrap">
            <ul class="hm-slider-wrap__bg" id="hm-slider">
                <?php foreach ($slides as $key => $item): ?>
                    <li style="background-image: url(<?= Url::to('@web/images/').$item->image ?>)"></li>
                <?php endforeach ?>
            </ul>
            <div class="hm-slider">
                <div class="container-content">
                    <ul class="hm-slider__caption" id="hm-slider-caption">
                        <?php foreach ($slides as $key => $item): ?>
                            <li class="uk-text-center">
                                <?= Html::img('@web/images/'.$item->slidelogo) ?>
                                <h3 class="hm-slider__title"><?= $item->title ?></h3>
                                <div class="hm-slider__caption"><?= $item->caption ?></div>
                            </li>
                        <?php endforeach ?>
                    </ul>
                    <div class="hm-slider__pager" id="bx-pager">
                        <?php foreach ($slides as $key => $item): ?>
                            <?= Html::a('', ['#'],['class'=>'hm-slider__dotnav','data-slide-index'=>$key]) ?>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="hm-first-screen__bottom">
            <div class="advantages">
                <div class="container-content">
                    <div class="advantages__grid uk-flex uk-flex-wrap uk-flex-center">
                        <div class="advantages__item-wrap">
                            <div class="advantages__item">
                                <div class="advantages__ico"><?= Html::img('@web/images/1.png') ?></div>
                                <div class="advantages__name">Выгода по трейд-ин</div>
                            </div>
                        </div>
                        <div class="advantages__item-wrap">
                            <div class="advantages__item">
                                <div class="advantages__ico"><?= Html::img('@web/images/2.png') ?></div>
                                <div class="advantages__name">Утилизация 2016</div>
                            </div>
                        </div>
                        <div class="advantages__item-wrap">
                            <div class="advantages__item">
                                <div class="advantages__ico"><?= Html::img('@web/images/3.png') ?></div>
                                <div class="advantages__name">Автомобили в наличии и с ПТС</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-content">
                <div class="hm-first-screen__menu">
                    <?php echo Menu::widget([
                        'items' => [
                            ['label' => 'Акции', 'url' => ['/aktion/index'],'template' => '<a href="{url}" class="menu-main__link">{label}</a>','options' => ['class' => 'menu-main__item']],
                            ['label' => 'Модельный ряд', 'url' => ['/category/index'],'template' => '<a href="{url}" class="menu-main__link">{label}</a>','options' => ['class' => 'menu-main__item']],
                            ['label' => 'Кредит', 'url' => ['/page/credit'],'template' => '<a href="{url}" class="menu-main__link">{label}</a>','options' => ['class' => 'menu-main__item']],
                            ['label' => 'Сервис', 'url' => ['/page/service'],'template' => '<a href="{url}" class="menu-main__link">{label}</a>','options' => ['class' => 'menu-main__item']],
                            ['label' => 'Трейд-ин', 'url' => ['/car/index'],'template' => '<a href="{url}" class="menu-main__link">{label}</a>','options' => ['class' => 'menu-main__item']],
                            ['label' => 'Контакты', 'url' => ['/page/contacts'],'template' => '<a href="{url}" class="menu-main__link">{label}</a>','options' => ['class' => 'menu-main__item']],
                        ],
                        'activeCssClass'=>'active',
                        'options' => [
                            'class' => 'menu-main',
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hm-auto mainWrapper uk-flex">
    <div class="hm-auto__wrap uk-flex uk-flex-wrap">
        <?php foreach ($categories as $key => $model): ?>
            <div class="hm-auto-item uk-flex uk-flex-bottom" style="background-image:url(<?= Url::to('@web/images/').$model->image ?>)">
                <div class="hm-auto-item__cont">
                    <div class="hm-auto-item__name"><?= $model->title ?></div>
                    <div class="hm-auto-item__price">от <?= $model->costfrom ?> Р.</div>
                    <div class="hm-auto-item__car-body">
                        <ul class="hm-auto-item__car-body-list">
                            <?php foreach (Carcass::find()->where(['categoryId'=>$model->id])->all() as $key => $carcass): ?>
                                <li><?= Html::a($carcass->title, ['category/index','#'=>'anchor-'.$carcass->categoryId],['title'=>$carcass->title]) ?></li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>
<div class="hm-about">
    <div class="container-content container-content_padding-all">
        <div class="grid">
            <div class="col-l-4 col-m-12 uk-margin-bottom">
                <div class="hm-about__title"><?= $homePage->c11 ?></div> 
            </div>
            <div class="col-l-8 col-m-12">
                <div class="content">
                    <?= $homePage->block1 ?>
                </div>
            </div>
        </div>
    </div>
</div>