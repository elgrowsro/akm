<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Category;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Модельный ряд';
?>

<div class="mainWrapper mainWrapper_bg mainWrapper_height-auto" style="background-image: url(&quot;/images/slide.jpg&quot;)">
    <?= $this->render('//layouts/header_nav') ?>
    <div class="page-title">
        <div class="container-content">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item"><a href="/" target="">Главная</a></li> -
                <li class="breadcrumbs__item"><span>Модельный ряд</span></li>
            </ul>
            <h1 class="page-title__h1">Модельный ряд</h1>
        </div>
    </div>
    <div class="container-content container-content_padding-all uk-padding-top-remove">
        <div class="uk-grid">
            <div class="uk-width-large-3-10 uk-width-xlarge-2-10">
                <div class="nav-page uk-margin-bottom" data-uk-sticky="{media: 960}">
                    <ul class="menu-nav" data-uk-scrollspy-nav="{closest:'li', smoothscroll:true}">
                        <li class="menu-nav__item"><a class="menu-nav__link" href="#anchor-0">Характеристики</a></li>
                        <li class="menu-nav__item"><a class="menu-nav__link" href="#anchor-1">Описание и фото</a></li>
                        <li class="menu-nav__item"><a class="menu-nav__link" href="#anchor-2">Акции на эту модель</a></li>
                        <li class="menu-nav__item"><a class="menu-nav__link" href="#anchor-3">Запись на тест-драйв</a></li>
                    </ul>
                </div>
            </div>
            <div class="uk-width-large-7-10 uk-width-xlarge-8-10">
                <div class="model-char uk-float-right" id="anchor-0">
                    <?= DetailView::widget([
                        'model' => $model,
                        'options'=>['class'=>'model-char__tbl'],
                        'attributes' => [
                            'typeCarcass',
                            'engineCapacity',
                            'power',
                            'transmission',
                            'gear',
                            'maxSpeed',
                            'acceleration',
                            'fuelConsumption',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-content container-content_padding-all">
    <div class="uk-grid">
        <div class="uk-width-large-3-10 uk-width-xlarge-2-10"></div>
        <div class="uk-width-large-7-10 uk-width-xlarge-8-10">
            <div class="model-caption-and-foto" id="anchor-1">
                <div class="title">Описание
                    <br> и фото</div>
                <div class="contet">
                    <p><?= $model->fulltext ?></p>
                </div>
                <div class="auto-fotogallery">
                    <ul class="switch" data-uk-switcher="{connect:'#auto-fotogallery-box', animation: 'fade'}">
                        <li class="switch__item"><a class="switch__link">Экстерьер</a></li>
                        <li class="switch__item"><a class="switch__link">Интерьер</a></li>
                    </ul>
                    <div class="uk-switcher" id="auto-fotogallery-box">
                        <div class="switch-content__item">
                            <?= ListView::widget([
                              'dataProvider' => $exterierProvider,
                              'itemView' => '_exterier',
                              'options'=>['class' => 'auto-fotogallery__list'],
                              'layout' => "{items}",
                            ]) ?>
                        </div>
                        <div class="switch-content__item">
                            <?= ListView::widget([
                              'dataProvider' => $interierProvider,
                              'itemView' => '_interier',
                              'options'=>['class' => 'auto-fotogallery__list'],
                              'layout' => "{items}",
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="model-actions bg-cover" style="background-image:url('/images/bg-model-action.jpg')">
    <div class="container-content container-content_padding-all">
        <div class="uk-grid">
            <div class="uk-width-large-3-10 uk-width-xlarge-2-10"></div>
            <div class="uk-width-large-7-10 uk-width-xlarge-8-10" id="anchor-2">
                <div class="title">Акции</div>
                <p>Повседневная практика показывает, что сложившаяся структура организации обеспечивает широкому кругу (специалистов) участие в формировании существенных финансовых и административных условий развития.</p>
                <div class="model-actions__slider">
                    <div class="actions-infobox-slider">
                        <div id="actions-infobox-slider">
                            <?php if ($aktions != NULL): ?>
                                <?php foreach ($aktions as $key => $aktion): ?>
                                    <div class="actions-infobox__cont">
                                        <div class="uk-grid uk-grid-medium">
                                            <div class="uk-width-large-4-10 uk-text-center">
                                                <div class="countdown" data-enddate="<?= date('Y, m, d',$aktion->enddate) ?>"></div>
                                                <h2 class="actions-infobox__title"><?= $aktion->title ?></h2>
                                                <p class="actions-infobox__anons"><?php $text = substr($aktion->description,0,200); echo $text."..."; ?></p>
                                                <?= Html::a('Подробнее', ['aktion/view','id'=>$aktion->id], ['class' => 'btn btn_ghost uk-button','target'=>'_blank']) ?>
                                            </div>
                                            <div class="uk-width-large-6-10">
                                                <?= Html::img('@web/images/'.$aktion->imginfobox,['alt'=>$aktion->title]) ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach ?>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="model-test-drive bg-cover">
    <div class="model-test-drive__bg" style="background-image:url('/images/bg-model-action.jpg')"></div>
    <div class="container-content">
        <div class="uk-grid">
            <div class="uk-width-large-3-10 uk-width-xlarge-2-10"></div>
            <div class="uk-width-large-7-10 uk-width-xlarge-8-10" id="anchor-3">
                <div class="uk-clearfix uk-grid uk-grid-divider">
                    <div class="uk-width-medium-1-2">
                        <div class="uk-float-left model-test-drive__form-box">
                            <div class="title"><span class="font">Записаться</span> на тест-драйв</div>
                            <div class="form form_transparent uk-form uk-form-stacked">
                                <?php $form = ActiveForm::begin(['id'=>'testdrive-form']); ?>
                                    <div class="uk-form-row">
                                        <?= $form->field($testdrive, 'name')->textInput(['class' => 'uk-width-1-1','placeholder'=>'Введите ваше имя...'])->label('Ваше имя',['class'=>'uk-form-label']) ?>
                                    </div>
                                    <div class="uk-form-row">
                                        <?= $form->field($testdrive, 'phone')->textInput(['class' => 'uk-width-1-1 js-mask-phone','placeholder'=>'+7 (900) 000-00-00'])->label('Контактный телефон',['class'=>'uk-form-label']) ?>
                                    </div>
                                    <div class="uk-form-row">
                                        <?= $form->field($testdrive, 'car')
                                            ->dropDownList(ArrayHelper::map(Category::find()->all(), 'title', 'title'),['class' => 'uk-width-1-1'])
                                            ->label('Выберите автомобиль',['class'=>'uk-form-label']) ?>
                                    </div>
                                    <div class="uk-form-row">
                                        <?= Html::submitButton('Записаться на тест-драйв', ['class'=>'btn btn_ghost uk-button uk-width-1-1']) ?>
                                    </div>
                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-2">
                        <div class="uk-float-right model-test-drive__form-box">
                            <div class="title"><span class="font">Бесплатный</span> звонок</div>
                            <div class="form form_transparent uk-form uk-form-stacked">
                                <?php $form = ActiveForm::begin(['id'=>'call-form']); ?>
                                    <div class="uk-form-row">
                                        <?= $form->field($call, 'name')->textInput(['class' => 'uk-width-1-1','placeholder'=>'Введите ваше имя...'])->label('Ваше имя',['class'=>'uk-form-label']) ?>
                                    </div>
                                    <div class="uk-form-row">
                                        <?= $form->field($call, 'phone')->textInput(['class' => 'uk-width-1-1 js-mask-phone','placeholder'=>'+7 (900) 000-00-00'])->label('Контактный телефон',['class'=>'uk-form-label']) ?>
                                    </div>
                                    <div class="uk-form-row">
                                        <?= Html::submitButton('Перезвоните мне', ['class'=>'btn btn_ghost uk-button uk-width-1-1']) ?>
                                    </div>
                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
