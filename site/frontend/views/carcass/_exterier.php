<?php
use yii\helpers\Html;
?>
<figure class="auto-fotogallery__item uk-overlay uk-overlay-hover">
	<?= Html::a(Html::img('@web/images/'.$model->image,['alt'=>'','width'=>'180px','height'=>'113px']).
	        '<figcaption class="uk-overlay-panel uk-overlay-fade uk-overlay-background uk-flex uk-flex-center uk-flex-middle uk-text-center">
	            <div><i class="uk-icon-eye"></i>
	                <div>Посмотреть</div>
	            </div>
	        </figcaption>', 
	    	'@web/images/'.$model->image, ['data-uk-lightbox' => '{group:"foto-group"}']) ?>
</figure>