<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\Menu;

?>

<?php echo Menu::widget([
    'items' => [
        ['label' => 'Характеристики', 'url' => ['view', 'id' => $model->id,'#'=>'anchor-0'],'template' => '<a href="{url}" class="menu-nav__link">{label}</a>','options' => ['class' => 'menu-nav__item']],
        ['label' => 'Описание и фото', 'url' => ['view', 'id' => $model->id,'#'=>'anchor-1'],'template' => '<a href="{url}" class="menu-nav__link">{label}</a>','options' => ['class' => 'menu-nav__item']],
        ['label' => 'Акции на эту модель', 'url' => ['view', 'id' => $model->id,'#'=>'anchor-2'],'template' => '<a href="{url}" class="menu-nav__link">{label}</a>','options' => ['class' => 'menu-nav__item']],
        ['label' => 'Запись на тест-драйв', 'url' => ['view', 'id' => $model->id,'#'=>'anchor-3'],'template' => '<a href="{url}" class="menu-nav__link">{label}</a>','options' => ['class' => 'menu-nav__item']],
    ],
    'activeCssClass'=>'active',
    'options' => [
        'class' => 'menu-nav',
        'data-uk-scrollspy-nav' => '{closest:"li", smoothscroll:true}',
    ],
]); ?>