<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;
use yii\widgets\Menu;
use common\models\Page;
use common\models\Category;
use frontend\models\ContactForm;
use yii\helpers\ArrayHelper;

use yii\bootstrap\ActiveForm;

$data = Page::findOne(4);
$call = new ContactForm();
$testdrive = new ContactForm();
/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Yii::$app->name ?></title>
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl.'/fav.png'; ?>">
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>

        <?= Alert::widget() ?>
        <?= $content ?>
        <div class="uk-modal modal" id="modal-call">
            <div class="uk-modal-dialog">
                <a class="uk-modal-close uk-close uk-close-alt" href=""></a>
                <div class="modal__img uk-hidden-small" style="background-image: url('/images/modal-call-img.jpg')"></div>
                <div class="modal__right">
                    <div class="form uk-form uk-form-stacked">
                        <?php $form = ActiveForm::begin(['action' =>['site/call'], 'id' => 'contact-form', 'method' => 'post',]); ?>
                            <div class="uk-form-row">
                                <?= $form->field($call, 'name')->textInput(['class' => 'uk-width-1-1','placeholder'=>'Введите ваше имя...'])->label('Ваше имя',['class'=>'uk-form-label']) ?>
                            </div>
                            <div class="uk-form-row">
                                <?= $form->field($call, 'phone')->textInput(['class' => 'uk-width-1-1 js-mask-phone','placeholder'=>'+7 (900) 000-00-00'])->label('Контактный телефон',['class'=>'uk-form-label']) ?>
                            </div>
                            <div class="uk-form-row">
                                <?= Html::submitButton('Перезвоните мне', ['class'=>'btn uk-button uk-button-primary uk-width-1-1']) ?>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
                <div class="uk-modal-caption">Бесплатный <b>звонок</b></div>
            </div>
        </div>
        <div class="uk-modal modal" id="modal-map">
            <div class="uk-modal-dialog uk-modal-dialog-lightbox modal__dialog-width-auto">
                <a class="uk-modal-close uk-close uk-close-alt" href=""></a>
                <div class="modal-map">
                    <div class="container-content container-content_padding-all">
                        <div class="h1">Схема проезда</div>
                        <div class="map-switch js-map-switch">
                            <a class="map-switch__item uk-margin-small uk-vertical-align" href="" style="background-image: url('/images/bg-map-switch-1.jpg')">
                                <div class="uk-vertical-align-bottom">AMK-Екатеринбург</div>
                            </a>
                            <a class="map-switch__item uk-margin-small uk-vertical-align" href="" style="background-image: url('/images/bg-map-switch-2.jpg')">
                                <div class="uk-vertical-align-bottom">Леруа Мерлен</div>
                            </a>
                        </div>
                    </div>
                    <div class="map">
                        <?= $data->c31 ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-modal modal" id="modal-testdrive">
            <div class="uk-modal-dialog">
                <a class="uk-modal-close uk-close uk-close-alt" href=""></a>
                <div class="modal__img uk-hidden-small" style="background-image: url('/images/modal-test-img.jpg')"></div>
                <div class="modal__right">
                    <div class="form uk-form uk-form-stacked">
                        <?php $form = ActiveForm::begin(['action' =>['site/testdrive'], 'id' => 'testdrive-form', 'method' => 'post',]); ?>
                            <div class="uk-form-row">
                                <?= $form->field($testdrive, 'name')->textInput(['class' => 'uk-width-1-1','placeholder'=>'Введите ваше имя...'])->label('Ваше имя',['class'=>'uk-form-label']) ?>
                            </div>
                            <div class="uk-form-row">
                                <?= $form->field($testdrive, 'phone')->textInput(['class' => 'uk-width-1-1 js-mask-phone','placeholder'=>'+7 (900) 000-00-00'])->label('Контактный телефон',['class'=>'uk-form-label']) ?>
                            </div>
                            <div class="uk-form-row">
                                <?= $form->field($testdrive, 'car')
                                    ->dropDownList(ArrayHelper::map(Category::find()->all(), 'title', 'title'),['class' => 'uk-width-1-1'])
                                    ->label('Выберите автомобиль',['class'=>'uk-form-label']) ?>
                            </div>
                            <div class="uk-form-row">
                                <?= Html::submitButton('Записаться на тест-драйв', ['class'=>'btn uk-button uk-width-1-1']) ?>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
                <div class="uk-modal-caption">Записаться на <b>тест-драйв</b></div>
            </div>
        </div>
        <div class="footWrap"></div>
        <footer class="footer">
            <div class="container-content">
                <div class="footer__row uk-flex">
                    <div class="footer__left">
                        <div class="footer__soc uk-float-right">
                            <?= Html::a('<i class="uk-icon-vk"></i>',$data->c12, ['target' => '_blank', 'class' => 'footer__soc-link']) ?>
                        </div>
                        <div class="footer__adres">
                            <?= $data->c23 ?>
                        </div>
                    </div>
                    <div class="footer__right">
                        <?php echo Menu::widget([
                            'items' => [
                                ['label' => 'Granta', 'url' => ['/category/index'],'template' => '<a href="{url}#anchor-1" class="footer-menu__link">{label}</a>','options' => ['class' => 'footer-menu__item']],
                                ['label' => 'Kalina', 'url' => ['/category/index'],'template' => '<a href="{url}#anchor-2" class="footer-menu__link">{label}</a>','options' => ['class' => 'footer-menu__item']],
                                ['label' => 'Priora', 'url' => ['/category/index'],'template' => '<a href="{url}#anchor-3" class="footer-menu__link">{label}</a>','options' => ['class' => 'footer-menu__item']],
                                ['label' => 'Vesta', 'url' => ['/category/index'],'template' => '<a href="{url}#anchor-4" class="footer-menu__link">{label}</a>','options' => ['class' => 'footer-menu__item']],
                                ['label' => 'Xray', 'url' => ['/category/index'],'template' => '<a href="{url}#anchor-5" class="footer-menu__link">{label}</a>','options' => ['class' => 'footer-menu__item']],
                                ['label' => 'Largus', 'url' => ['/category/index'],'template' => '<a href="{url}#anchor-6" class="footer-menu__link">{label}</a>','options' => ['class' => 'footer-menu__item']],
                                ['label' => '4x4', 'url' => ['/category/index'],'template' => '<a href="{url}#anchor-7" class="footer-menu__link">{label}</a>','options' => ['class' => 'footer-menu__item']],
                            ],
                            'activeCssClass'=>'active',
                            'options' => [
                                'class' => 'menu-sidebar', 
                            ],
                        ]); ?>
                        <p class="footer__txt">
                            <?= $data->block2 ?>
                        </p>
                    </div>
                </div>
            </div>
        </footer>
        <div class="uk-modal modal" id="modal-spinner">
            <div class="uk-modal-dialog uk-modal-dialog-lightbox modal-in">
                <a class="uk-modal-close uk-close uk-close-alt" href=""></a>
                <div class="uk-hidden" id="modal-dialog-content"></div>
                <div class="uk-modal-caption"></div>
                <div class="uk-modal-spinner"></div>
            </div>
        </div>
        <div class="sidebar uk-offcanvas" id="sidebar">
            <div class="uk-offcanvas-bar">
                <div class="sidebar__wrapp uk-flex">
                    <div class="sidebar__bar uk-flex uk-flex-column uk-flex-space-between">
                        <div></div>
                        <div class="sidebar__logo">
                            <a href="/"><img src="/images/logo.png" alt=""></a>
                        </div>
                        <nav class="sidebar__nav">
                            <?= $this->render('//layouts/sidebar_nav') ?>
                        </nav>
                        <div></div>
                        <div class="sidebar__bottom">
                            <div class="uk-margin"><a href="tel:+7 (343) 211 10 00 "><i class="icon sprite-i-phone-vol"></i> +7 (343) 211 10 00</a></div>
                            <div class="uk-margin">
                                <button class="uk-button" type="button" data-uk-modal="{target:'#modal-call'}">Бесплатный звонок</button>
                            </div>
                            <div class="uk-margin"><a href="#modal-map" data-uk-modal title="title"><i class="uk-icon-map-marker"></i> Как проехать?</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
