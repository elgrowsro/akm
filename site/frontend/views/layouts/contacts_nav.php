<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\Menu;
use common\models\Page;

$data = Page::findOne(4);
?>
<header class="header header_bg">
    <div class="container-content">
        <div class="uk-flex uk-flex-space-between uk-flex-wrap uk-flex-middle">
            <div class="header__left">
                <div class="header__logo">
                    <?= Html::a(Html::img('@web/images/logo.png', ''),Yii::$app->homeUrl) ?>
                </div>
                <nav class="header__nav">
                    <?php echo Menu::widget([
                        'items' => [
                            ['label' => 'Акции', 'url' => ['/aktion/index'],'template' => '<a href="{url}" class="menu-main__link">{label}</a>','options' => ['class' => 'menu-main__item']],
                            ['label' => 'Модельный ряд', 'url' => ['/category/index'],'template' => '<a href="{url}" class="menu-main__link">{label}</a>','options' => ['class' => 'menu-main__item']],
                            ['label' => 'Кредит', 'url' => ['/page/credit'],'template' => '<a href="{url}" class="menu-main__link">{label}</a>','options' => ['class' => 'menu-main__item']],
                            ['label' => 'Сервис', 'url' => ['/page/service'],'template' => '<a href="{url}" class="menu-main__link">{label}</a>','options' => ['class' => 'menu-main__item']],
                            ['label' => 'Трейд-ин', 'url' => ['/car/index'],'template' => '<a href="{url}" class="menu-main__link">{label}</a>','options' => ['class' => 'menu-main__item']],
                            ['label' => 'Контакты', 'url' => ['/page/contacts'],'template' => '<a href="{url}" class="menu-main__link">{label}</a>','options' => ['class' => 'menu-main__item']],
                        ],
                        'activeCssClass'=>'active',
                        'options' => [
                            'class' => 'menu-main',
                        ],
                    ]); ?>
                </nav> 
            </div>
            <div class="header__right"><?= Html::a('<i class="icon sprite-i-phone-vol"></i> '.$data->c13,'tel:'.$data->c13) ?>
            <?= Html::tag('button', 'Бесплатный звонок', ['class' => 'uk-button','data-uk-modal' => '{target:\'#modal-call\'}']) ?>
            <a href="#modal-map" data-uk-modal title="title"><i class="uk-icon-map-marker"></i> Как проехать?</a>
            <a class="sidebar-tgl" href="#sidebar" data-uk-offcanvas>Меню <i class="uk-icon-bars"></i></a></div>
        </div>
    </div>
</header>