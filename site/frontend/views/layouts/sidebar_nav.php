<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\Menu;

?>

<?php echo Menu::widget([
    'items' => [
        ['label' => 'Акции', 'url' => ['/aktion/index'],'template' => '<a href="{url}" class="menu-sidebar__link">{label}</a>','options' => ['class' => 'menu-sidebar__item']],
        ['label' => 'Модельный ряд', 'url' => ['/category/index'],'template' => '<a href="{url}" class="menu-sidebar__link">{label}</a>','options' => ['class' => 'menu-sidebar__item']],
        ['label' => 'Кредит', 'url' => ['/page/credit'],'template' => '<a href="{url}" class="menu-sidebar__link">{label}</a>','options' => ['class' => 'menu-sidebar__item']],
        ['label' => 'Сервис', 'url' => ['/page/service'],'template' => '<a href="{url}" class="menu-sidebar__link">{label}</a>','options' => ['class' => 'menu-sidebar__item']],
        ['label' => 'Трейд-ин', 'url' => ['/car/index'],'template' => '<a href="{url}" class="menu-sidebar__link">{label}</a>','options' => ['class' => 'menu-sidebar__item']],
        ['label' => 'Контакты', 'url' => ['/page/contacts'],'template' => '<a href="{url}" class="menu-sidebar__link">{label}</a>','options' => ['class' => 'menu-sidebar__item']],
    ],
    'activeCssClass'=>'active',
    'options' => [
        'class' => 'menu-sidebar',
    ],
]); ?>
