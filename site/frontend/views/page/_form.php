<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'c11')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'c12')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'c13')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'c21')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'c22')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'c23')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'c31')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'c32')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'block1')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'block2')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'block3')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
