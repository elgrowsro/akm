<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
/* @var $model common\models\Aktion */

?>
<div class="mainWrapper mainWrapper_bg mainWrapper_height-auto" style="background-image: url(&quot;/images/slide.jpg&quot;)">
	<?= $this->render('//layouts/header_nav') ?>
    <div class="page-title">
        <div class="container-content">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item"><a href="/" target="">Главная</a></li> -
                <li class="breadcrumbs__item"><span>Кредит</span></li>
            </ul>
            <h1 class="page-title__h1">Кредит</h1></div>
    </div>
    <div class="container-content container-content_padding-all uk-padding-top-remove">
        <p><b>Выгодные условия кредита</b>
            <br> Автосалон "АМК Екатеринбург" представляет широкий спектр программ автокредитования.
        </p>
    </div>
</div>
<div class="uk-overflow-hidden">
    <div class="container-content container-content_padding-all uk-padding-bottom-remove content">
        <p><?= $model->block1 ?></p>
        <div class="warning-line uk-margin"><?= $model->c12 ?></div>
        <div class="grid">
            <div class="col-l-4 col-m-12">
                <div class="title"><?= $model->c11 ?></div>
            </div>
            <div class="col-l-8 col-m-12">
                <?= $model->block2 ?>
                <div class="muted-line uk-margin-top"><b style="font-size: 16px;">
                    <?= $model->block3 ?>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-cover" style="background-image:url('/images/bg-model-action.jpg')">
        <div class="container-content container-content_padding-all">
            <div class="form form_transparent uk-form uk-form-stacked">
                <?php $form = ActiveForm::begin(['id'=>'call-form']); ?>
                    <div class="uk-form-row uk-grid uk-grid-medium">
                        <div class="uk-form-controls uk-margin-bottom">
                            <?= $form->field($call, 'name')->textInput(['class' => 'uk-form-width-large','placeholder'=>'Введите ваше имя...'])->label('Ваше имя',['class'=>'uk-form-label']) ?>
                        </div>
                        <div class="uk-form-controls uk-margin-bottom">
                            <?= $form->field($call, 'phone')->textInput(['class' => 'uk-form-width-large js-mask-phone','placeholder'=>'+7 (900) 000-00-00'])->label('Контактный телефон',['class'=>'uk-form-label']) ?>
                        </div>
                        <div class="uk-form-controls uk-margin-bottom">
                            <label class="uk-form-label">&nbsp;</label>
                            <?= Html::submitButton('Перезвонить мне', ['class'=>'btn uk-button uk-form-width-large']) ?>
                        </div>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>