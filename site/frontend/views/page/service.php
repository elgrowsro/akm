<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Category;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="mainWrapper mainWrapper_bg" style="background-image: url(&quot;/images/slide.jpg&quot;)">
	<?= $this->render('//layouts/header_nav') ?>
    <div class="page-title">
        <div class="container-content">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item"><a href="/" target="">Главная</a></li> -
                <li class="breadcrumbs__item"><span>Сервис</span></li>
            </ul>
            <h1 class="page-title__h1">Сервис</h1></div>
    </div>
    <div class="container-content container-content_padding-all uk-padding-top-remove">
        <div class="uk-grid uk-grid-medium" data-uk-grid-match="{target: '.bg-box_dark'}">
            <div class="uk-width-large-3-10 uk-flex uk-margin-bottom">
                <div class="bg-box_dark uk-flex uk-width-1-1">
                    <div class="form form_transparent uk-form uk-form-stacked uk-width-1-1">
                        <?php $form = ActiveForm::begin(['id'=>'testdrive-form']); ?>
                            <div class="uk-form-row">
                                <?= $form->field($testdrive, 'name')->textInput(['class' => 'uk-width-1-1','placeholder'=>'Введите ваше имя...'])->label('Ваше имя',['class'=>'uk-form-label']) ?>
                            </div>
                            <div class="uk-form-row">
                                <?= $form->field($testdrive, 'phone')->textInput(['class' => 'uk-width-1-1 js-mask-phone','placeholder'=>'+7 (900) 000-00-00'])->label('Контактный телефон',['class'=>'uk-form-label']) ?>
                            </div>
                            <div class="uk-form-row">
                                <?= $form->field($testdrive, 'car')
                                    ->dropDownList(ArrayHelper::map(Category::find()->all(), 'title', 'title'),['class' => 'uk-width-1-1'])
                                    ->label('Выберите автомобиль',['class'=>'uk-form-label']) ?>
                            </div>
                            <div class="uk-form-row">
                                <?= Html::submitButton('Записаться на тест-драйв', ['class'=>'uk-button uk-width-1-1']) ?>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
            <div class="uk-width-large-7-10">
                <div class="uk-grid uk-grid-collapse uk-grid-width-large-1-3">
                    <div>
                        <div class="bg-box_dark bg-box-info uk-flex uk-flex-center uk-flex-middle uk-text-center">
                            <div>
                                <div class="bg-box-info__title">Автомобили отечественного производства, находящиеся на гарантии LADA:</div>
                                <p>Техническое обслуживание -
                                    <br><span class="uk-text-primary"><?= $model->c11 ?> рублей</span></p>
                                <p>Слесарно-диагностические работы -
                                    <br><span class="uk-text-primary"><?= $model->c12 ?> рублей</span></p>
                                <p>Жестяно-покрасочные работы -
                                    <br><span class="uk-text-primary"><?= $model->c13 ?> рублей</span></p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="bg-box_dark bg-box-info uk-flex uk-flex-center uk-flex-middle uk-text-center">
                            <div>
                                <div class="bg-box-info__title">Автомобили отечественного производства, постгарантия LADA:</div>
                                <p>Техническое обслуживание -
                                    <div class="uk-text-primary"><?= $model->c21 ?> рублей</div>
                                </p>
                                <p>Слесарно-диагностические работы -
                                    <div class="uk-text-primary"><?= $model->c22 ?> рублей</div>
                                </p>
                                <p>Жестяно-покрасочные работы -
                                    <div class="uk-text-primary"><?= $model->c23 ?> рублей</div>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="bg-box_dark bg-box-info uk-flex uk-flex-center uk-flex-middle uk-text-center">
                            <div>
                                <div class="bg-box-info__title">Автомобили импортного производства:</div>
                                <p>Техническое обслуживание -
                                    <div class="uk-text-primary"><?= $model->c31 ?> рублей</div>
                                </p>
                                <p>Жестяно-покрасочные работы -
                                    <div class="uk-text-primary"><?= $model->c32 ?> рублей</div>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="uk-overflow-hidden">
    <div class="container-content container-content_padding-all">
        <div class="content">
            <?= $model->block1 ?>
            <div class="muted-line uk-margin">
                <div class="uk-grid">
                    <div class="uk-width-large-3-10">
                        <div class="title title_small">Сервисный центр (СТО)</div>
                    </div>
                    <div class="uk-width-large-7-10">
                        <?= $model->block2 ?>
                    </div>
                </div>
            </div>
            <div class="uk-grid">
                <div class="uk-width-large-3-10">
                    <div class="title title_small">Автосервис автоцентра АМК-Екатеринбург оказывает следующие виды услуг:</div>
                </div>
                <div class="uk-width-large-7-10">
                    <h5><b>Диагностика</b></h5>
                    <?= $model->block3 ?>
                </div>
            </div>
        </div>
    </div>
</div>