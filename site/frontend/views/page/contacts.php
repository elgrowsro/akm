<div class="mainWrapper contacts-page">
    <?= $this->render('//layouts/contacts_nav') ?>
    <div class="page-title">
        <div class="container-content">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item"><a href="/" target="">Главная</a></li> -
                <li class="breadcrumbs__item"><span>Контакты</span></li>
            </ul>
            <h1 class="page-title__h1">Контакты</h1>
        </div>
    </div>
    <div class="container-content container-content_padding-all uk-padding-top-remove">
        <div class="contact-info content">
            <div class="bg-box uk-margin-small">
                <?= $model->block1 ?>
            </div>
            <div class="bg-box uk-margin-small">
                <?= $model->block2 ?>
            </div>
            <div class="bg-box uk-margin-small">
                <?= $model->block3 ?>
            </div>
        </div>
        <div class="map">
            <?= $model->c11 ?>
        </div>
    </div>
</div>


