<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<figure class="auto-in__item">
	<?= Html::a('<div class="auto-in__img">'.Html::img('@web/images/'.$model->image,['alt'=>'']).
	        '</div><figcaption class="auto-in__caption">'.$model->title.' '.$model->year.' г. / '.$model->mileage.' км /<br> '.$model->cost.' Р</figcaption>', 
	    	Url::to('/car/view?id='.$model->id),['class' => 'auto-in__link js-modal-trade-in','title'=>$model->title,'id'=>'contact-modal']) ?>
</figure>