<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\widgets\ListView;
use yii\bootstrap\Modal;
use yii\helpers\Url;

?>
<div class="mainWrapper mainWrapper_bg" style="background-image: url(&quot;/images/slide.jpg&quot;)">
    <?= $this->render('//layouts/header_nav') ?>
    <div class="page-title">
        <div class="container-content">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item"><a href="/" target="">Главная</a></li> -
                <li class="breadcrumbs__item"><span>Трейд-ин</span></li>
            </ul>
            <h1 class="page-title__h1">Трейд-ин</h1></div>
    </div>
    <div class="container-content container-content_padding-all uk-padding-top-remove">
        <div class="actions-infobox__cont">
            <div class="uk-grid uk-grid-medium">
                <?php if ($aktion != NULL): ?>
                    <div class="uk-width-large-4-10 uk-text-center">
                        <div class="countdown" data-enddate="<?= date('Y, m, d',$aktion->enddate) ?>"></div>
                        <h2 class="actions-infobox__title"><?= $aktion->title ?></h2>
                        <p class="actions-infobox__anons"><?php $text = substr($aktion->description,0,200); echo $text."..."; ?></p>
                        <?= Html::a('Подробнее', ['view','id'=>$aktion->id], ['class' => 'btn btn_ghost uk-button','target'=>'_blank']) ?>
                    </div>
                    <div class="uk-width-large-6-10">
                        <?= Html::img('@web/images/'.$aktion->imginfobox,['alt'=>$aktion->title]) ?>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>
    <div class="container-content container-content_padding-all">
        <div class="auto-in">
            <div class="title">Автомобили в наличии</div>
            <?= ListView::widget([
              'dataProvider' => $dataProvider,
              'itemView' => '_items',
              'options'=>['class' => 'auto-in__list uk-grid uk-grid-small uk-grid-width-large-1-4 uk-grid-width-medium-1-2 uk-grid-width-small-1-1'],
              'layout' => "{items}",
            ]) ?>
        </div>
    </div>
</div>
