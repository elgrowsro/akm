<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;
use frontend\models\ContactForm;

?>
<div class="mainWrapper mainWrapper_lightbox" id="contact-modal">
    <div class="mainWrapper_lightbox__box">
        <div class="slidecarousel">
            <ul id="slidecarousel">
                <?php foreach ($images as $key => $item): ?>
                    <li><?= Html::img('@web/images/'.$item->image,['alt'=>'']) ?></li>
                <?php endforeach ?>
            </ul>
            <div class="slidecarousel__pager">
                <div id="slidecarousel-bx-pager">
                    <?php foreach ($images as $key => $item): ?>
                        <?= Html::a(Html::img('@web/images/'.$item->image,['width'=>'128px','height'=>'80px']),[''],['data-slide-index'=>$key]) ?>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
        <div class="uk-grid uk-grid-divider" data-uk-grid-match="data-uk-grid-match">
            <div class="uk-width-small-6-10">
                <div class="padding">
                    <div class="uk-grid uk-grid-medium uk-grid-width-small-1-2">
                        <div class="uk-margin-small-bottom">Год выпуска: <b><?= $model->year ?></b></div>
                        <div class="uk-margin-small-bottom">Двигатель: <b><?= $model->engineCapacity ?></b></div>
                        <div class="uk-margin-small-bottom">Пробег: <b><?= $model->mileage ?> км</b></div>
                        <div class="uk-margin-small-bottom">КПП: <b><?= $model->gear ?></b></div>
                    </div>
                </div>
            </div>
            <div class="uk-width-small-4-10 uk-flex uk-flex-middle uk-flex-right">
                <div class="padding">
                    <div class="price uk-text-primary"><?= $model->cost ?> Р.</div>
                </div>
            </div>
        </div>
    </div>
    <div class="mainWrapper_lightbox__box">
        <div class="form uk-form uk-form-stacked padding">
            <form id="" method="" action="">
                <div class="uk-form-row uk-grid uk-grid-width-small-1-2">
                    <div class="uk-form-controls">
                        <label class="uk-form-label">Вас зовут</label>
                        <input class="uk-width-1-1" type="text" placeholder="Введите ваше имя..." />
                    </div>
                    <div class="uk-form-controls">
                        <label class="uk-form-label">Контактный телефон</label>
                        <input class="uk-width-1-1 js-mask-phone" type="text" placeholder="+7 (900) 000-00-00" />
                    </div>
                </div>
                <div class="uk-form-row">
                    <button class="uk-button uk-width-1-1" type="submit">Перезвонить мне</button>
                </div>
            </form>
        </div>
    </div>
</div>

