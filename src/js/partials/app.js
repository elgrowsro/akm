(function(){
  $(document).ready(function(){
      var $hmSlider = $('#hm-slider').bxSlider({
          pagerCustom: '#bx-pager',
          controls: false,
          auto: true
      });
      var $hmSliderCaption = $('#hm-slider-caption').bxSlider({
          pagerCustom: '#bx-pager',
          mode: 'fade',
          nextText: '',
          prevText: '',
          auto: true,
          onSlideNext: function(){
              $hmSlider.goToNextSlide();
          },
          onSlidePrev: function(){
              $hmSlider.goToPrevSlide();
          },
      });

      //countdown http://tutorialzine.com/2011/12/countdown-jquery/
      var $countdown = $('.countdown');
      $countdown.each(function(){
          var $this= $(this),
          text = $this.data('enddate'),
          date = new Date(text.replace(/(\d+).(\d+).(\d+)/, '$3/$2/$1')),
          ts = date.getTime();
          $this.countdown({
              callback : function(days, hours, minutes, seconds){},
              timestamp : ts
          });
          $this.find('.countDays').append('<span class="countdown-hint">дней</span>');
          $this.find('.countHours').append('<span class="countdown-hint">часов</span>');
          $this.find('.countMinutes').append('<span class="countdown-hint">минут</span>');
      });


      //phone mask
      var $maskPhone = $('.js-mask-phone');
      $maskPhone.mask("+7 (999) 999-99-99");

      //
      var $inputTransparent = $('.form_transparent input');
      $inputTransparent.on('focus', function(){
          var $this = $(this);
          $this.prev('label').addClass('focus');
      });
      $inputTransparent.on('focusout', function(){
          var $this = $(this);
          $this.prev('label').removeClass('focus');
      });


      //MODAL
      var $modalDialog = $("#modal-dialog-content");
       $modalDialog.removeClass('uk-hidden');
      var $modalDialogCaption = $('#modal-spinner .uk-modal-caption')
      var $spinner = $(".uk-modal-spinner");
      var $modalSpinner = UIkit.modal("#modal-spinner");
      var $modalTradeIn = $('.js-modal-trade-in');
      $modalTradeIn.on('click', function(){
          var $this = $(this),
          href = $this.prop('href');
          title = $this.prop('title');
          $modalDialogCaption.html(title);
          $.ajax({
            dataType: 'html',
            url: href,
            success: function(data){
              $modalDialog.html(data);
            //   var $slideShow = $('#slidecarousel').bxSlider({
            //       'pagerCustom': '#slidecarousel-bx-pager',
            //       control: false,
            //       slideWidth: 600,
            //       onSlideNext: function(){
            //           $slidecarouselPager.goToNextSlide();
            //       },
            //       onSlidePrev: function(){
            //           $slidecarouselPager.goToPrevSlide();
            //       },
            //   });
            //   var $slidecarouselPager = $('#slidecarousel-bx-pager').bxSlider({
            //       slideWidth: 128,
            //       minSlides: 1,
            //       maxSlides: 4,
            //       slideMargin: 2,
            //       pager: false,
            //       nextText: '',
            //       prevText: '',
            //       onSlideNext: function(){
            //           $slideShow.goToNextSlide();
            //       },
            //       onSlidePrev: function(){
            //           $slideShow.goToPrevSlide();
            //       },
            //   });
                function slideshow() {
                var $slideShow = $('#slidecarousel').bxSlider({
                    'pagerCustom': '#slidecarousel-bx-pager',
                    control: false,
                    slideWidth: 1200,
                    onSlideNext: function(){
                        $slidecarouselPager.goToNextSlide();
                    },
                    onSlidePrev: function(){
                        $slidecarouselPager.goToPrevSlide();
                    },
                });
                var $slidecarouselPager = $('#slidecarousel-bx-pager').bxSlider({
                    slideWidth: 123,
                    minSlides: 1,
                    maxSlides: 9,
                    slideMargin: 2,
                    pager: false,
                    nextText: '',
                    prevText: '',
                    onSlideNext: function(){
                        $slideShow.goToNextSlide();
                    },
                    onSlidePrev: function(){
                        $slideShow.goToPrevSlide();
                    },
                });
            }
                // timeoutId = setTimeout(slideshow, 100);
                slideshow();
                $('body #contact-form').on('submit', function(){
                var flag = 0;
                var $this = $(this);
                var $inp = $this.find('input');

                $inp.each(function(){
                    var $this = $(this);
                    if ($this.val() == '') {
                        $this.next('.help-block-error').text('Это поле обязательно для заполнения');
                        flag = 1;
                    }

                });
                if (flag == 1) {
                    return false;
                }
            });
                //phone mask
    var $maskPhone = $('.js-mask-phone');
    $maskPhone.mask("+7 (999) 999-99-99");
                function show() {
                  $spinner.addClass('uk-hidden');
                }
                timeoutId = setTimeout(show, 100);
            },
            complete: function(){
              $modalDialog.removeClass('uk-hidden');
            //   function show() {
            //     $spinner.addClass('uk-hidden');
            //   }
            //   timeoutId = setTimeout(show, 100);
            }
          });
          $modalSpinner.show();
          return false;
      });

      //MAP SWITCH
      var $mapswitch = $('.js-map-switch a');
      $mapswitch.on('click', function(){
          var $this = $(this);
          $mapswitch.removeClass('active');
          $this.addClass('active');
          return false;
      });
  });
  $('#modal-map').on('show.uk.modal', function(){

  });
  $(window).load(function(){
      var $actionsInfoboxSlider = $('#actions-infobox-slider').bxSlider({
          auto: true
      });


  })
})(jQuery);
